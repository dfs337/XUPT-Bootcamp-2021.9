## Qt安装

从2020年1月开始，Qt在线安装时需要强制登录Qt账号，从5.15版本开始不再提供离线安装包。

安装时需要注册Qt账号并登录。

**注意**：在安装路径中不要出现空格和中文等特殊字符。

![image-20200509151235041](Qt%20Setup.assets/image-20200509151235041.png)

选择组件时根据情况选择需要安装的Qt库的版本（建议选择`MinGW 7.3.0 32-bit`，即GCC编译的32位库）。

![image-20200509151410152](Qt%20Setup.assets/image-20200509151410152.png)

如果上面选择了`MinGW 7.3.0 32-bit`版本的Qt库，还需要安装GCC编译工具链。在`Developer and Designer Tools`组中选择`MinGW 7.3.0 32-bit`，**注意这个是编译器，和上面选择的Qt库不是同一个组件。**

![image-20200509151835843](Qt%20Setup.assets/image-20200509151835843.png)

## 配置Qt Creator

![image-20210914095021914](Qt%20Setup.assets/image-20210914095021914.png)

![image-20210914095111550](Qt%20Setup.assets/image-20210914095111550.png)



## 创建C语言工程

![image-20210914095254479](Qt%20Setup.assets/image-20210914095254479.png)

工程所在路径中不能有中文字符或空格

![image-20210914095534265](Qt%20Setup.assets/image-20210914095534265.png)

build system选择cmake

![image-20210914095637820](Qt%20Setup.assets/image-20210914095637820.png)

