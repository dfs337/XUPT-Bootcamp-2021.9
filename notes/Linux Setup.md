## 在Debian Linux上安装C语言开发环境

1. 使用root用户登录虚拟机

```bash
# 安装sudo程序
apt install sudo
# 将普通用户添加到sudo组
usermod -G sudo student
```

2. 安装开发环境，使用普通用户登录

```bash
sudo apt install build-essential cmake manpages
```

3. 编译CMake工程

```bash
# 将Qt创建的cmake工程通过winscp拷贝到虚拟机上
# 进入工程目录
cd test
# 执行cmake
cmake .
# 执行make
make
# 运行程序
./test
```

